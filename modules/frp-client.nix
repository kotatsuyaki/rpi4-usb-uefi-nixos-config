# frp (fast reverse tunnel) client

{ pkgs, ... }:

let
  frp-client-cfg-file = pkgs.writeText "frp-client.ini" ''
    [common]
    server_addr = 45.63.11.139
    server_port = 49999
    authentication_method = token
    token = {{ .Envs.FRP_TOKEN }}

    [ssh]
    type = tcp
    local_ip = 127.0.0.1
    local_port = 22
    remote_port = 50022

    [testweb]
    type = tcp
    local_ip = 127.0.0.1
    local_port = 2010
    remote_port = 52010
  '';
in
{
  systemd.services.frp-client = {
      wantedBy = [ "multi-user.target" ]; 
      after = [ "network.target" ];
      description = "Start the frp client";
      serviceConfig = {
        ExecStart = "${pkgs.frp}/bin/frpc -c ${frp-client-cfg-file}";
        EnvironmentFile = "/var/lib/frp-client.env";
        Type = "simple";
        Restart = "always";
        RestartSec = 10;
        DynamicUser = true;
      };
   };
}
