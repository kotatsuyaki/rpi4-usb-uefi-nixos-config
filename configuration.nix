{ pkgs, lib, ... }:

{
  services.openssh = {
    enable = true;
  };
  
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAP09p944zBilDlZ+Y6vPLzWFQYVHA3Taf1alBLop5Cj akitaki@rtx3070"
  ];

  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.enable = true;
  boot.loader.grub.enable = false;

  fileSystems."/" = {
    device = "/dev/disk/by-label/RPIROOT";
    fsType = "btrfs";
    options = [ "compress=zstd" "discard" "noatime" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/RPIBOOT";
    fsType = "vfat";
  };

  swapDevices = [
    {
      device = "/dev/disk/by-label/RPISWAP";
    }
  ];

  boot.kernelParams = [
    "console=ttyS0,115200n8" "console=ttyAMA0,115200n8" "console=tty0" "cma=64M"
  ];
  
  boot.initrd.availableKernelModules = [
    # Allows early (earlier) modesetting for the Raspberry Pi
    "vc4"
    "bcm2835_dma"
    "i2c_bcm2835"
    
    # Maybe needed for SSD boot?
    "usb_storage"
    "xhci_pci"
    "usbhid"
    "uas"
  ];

  boot.supportedFilesystems = [ "btrfs" ];
  environment.systemPackages = [ pkgs.python3 ];

  boot.kernelPackages = pkgs.linuxPackages_latest;
  powerManagement.cpuFreqGovernor = "ondemand";
}
