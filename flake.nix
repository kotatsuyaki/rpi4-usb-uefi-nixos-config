{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  
  outputs = { self, nixpkgs, nixos-generators, ... }: {
    nixosConfigurations."rpi4" = nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      modules = [
        ./configuration.nix
        ./modules/frp-client.nix
      ];
    };
 
    packages.aarch64-linux = {
      rpiimg = nixos-generators.nixosGenerate {
        system = "aarch64-linux";
        modules = [
          ./configuration.nix
        ];
        format = "vmware";
        
        pkgs = nixpkgs.legacyPackages.aarch64-linux;
        lib = nixpkgs.legacyPackages.aarch64-linux.lib;
      };
    };
  };
}
